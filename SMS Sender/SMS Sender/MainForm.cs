﻿using System;
using System.IO.Ports;
using System.Windows.Forms;

namespace SMS_Sender
{
	public partial class MainForm : Form
	{
		private SerialPort _port;

		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, System.EventArgs e)
		{
			comboBoxPorts.Items.AddRange(SerialPort.GetPortNames());

			if (comboBoxPorts.Items.Count > 0)
			{
				comboBoxPorts.SelectedIndex = 0;
			}
			else
			{
				MessageBox.Show("No COM ports found on this PC.");
				buttonSend.Enabled = false;
			}

		}

		private void buttonSend_Click(object sender, System.EventArgs e)
		{
			_port.WriteLine($"PHONE:{textBoxPhone.Text}");
			_port.WriteLine($"MESSAGE:{textBoxMessage.Text}");
			_port.WriteLine("COMMAND:SEND");
		}

		public void AddInfo(string message)
		{
			listBoxReceived.Items.Insert(0, $"{DateTime.Now.ToLongTimeString()} {message}");

			if (listBoxReceived.Items.Count > 100)
			{
				listBoxReceived.Items.RemoveAt(100);
			}
		}

		private void buttonConnect_Click(object sender, EventArgs e)
		{
			_port?.Close();
			_port = new SerialPort { BaudRate = 19200, PortName = comboBoxPorts.Text };
			_port.DataReceived += _port_DataReceived;
			_port.Open();
		}

		private void _port_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			var data = _port.ReadLine();

			while (!string.IsNullOrEmpty(data))
			{
				var data1 = data;
				Action action = () => AddInfo(data1);
				BeginInvoke(action);

				if (!_port.IsOpen)
					break;

				if (_port.BytesToRead == 0)
					break;

				data = _port.ReadLine();
			}
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			_port?.Close();
		}
	}
}
